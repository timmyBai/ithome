# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy;

class IthomeItem(scrapy.Item):
    title = scrapy.Field();
    link = scrapy.Field();
    images = scrapy.Field();
    introduction = scrapy.Field();
    date = scrapy.Field();