import scrapy;
from ithome.items import IthomeItem;

print('\033[1;32;40m');

class itHomeSecuity(scrapy.Spider):
    name = 'security';
    
    def start_requests(self):
        start_urls = [
            'https://www.ithome.com.tw/security'
        ];

        yield scrapy.Request(start_urls[0], callback=self.parse)

    def parse(self, response):
        view = response.css('.main-container.container-fluid > .row-fluid > .main-content.span9 > .view');

        ithomeItem = IthomeItem();

        for item in view.css('.span4.channel-item > .item'):
            ithomeItem['title'] = item.css('.title > a::text').get();
            ithomeItem['link'] = 'https://www.ithome.com.tw' +  item.css('.title > a::attr("href")').get();
            ithomeItem['images'] = item.css('p.photo > a > img::attr("src")').get();
            ithomeItem['introduction'] = item.css('.summary::text').get();
            ithomeItem['date'] = item.css('.post-at::text').get() # 先 '-' 符號洗掉，轉換整數 - 19110000，再轉字串
            
            # insert mysql save
            yield ithomeItem;
            
        next_page = view.css('.pagination.pagination-centered > ul > .next.last > a::attr("href")').get();
        if next_page is not None:
            current_next_page = 'https://www.ithome.com.tw' + next_page;
            yield response.follow(current_next_page, self.parse);